/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Victor
 * Created: 02-may-2017
 */
DROP TABLE IF EXISTS Editoriales;
 CREATE TABLE IF NOT EXISTS Editoriales (
  IdEditorial int unsigned AUTO_INCREMENT PRIMARY KEY,
  Nombre     varchar(30),
  Direccion  varchar(50),
  Poblacion  varchar(25),
  CodPostal  char(5) ,
  Nif        char(9) Unique,
  Telefono  char(9),
  Fax        char(9),
  Email      varchar(40) unique
 -- CONSTRAINT Id_Editorial_PK PRIMARY KEY (IdEditorial)
);

CREATE TABLE IF NOT EXISTS Temas(
    IdTema int unsigned AUTO_INCREMENT PRIMARY KEY,
    Tema VARCHAR(30)
    --CONSTRAINT Id_Tema_PK PRIMARY KEY (IdTema)
);

CREATE TABLE Libros (
  `IdLibro` int unsigned AUTO_INCREMENT Primary Key,
--  `ISBN` char(13) unique,
--  `EAN` char(13) unique,
  `Titulo` varchar(80) NOT NULL,
  `Autores` varchar(80) NOT NULL,
  `IdEditorial` int(4) unsigned,
  `IdTema` int(2) unsigned,
  `PrecioCoste` decimal(4,2) DEFAULT '0.00',
  `PvpSinIva` decimal(4,2) DEFAULT '0.00',
  `StockActual` int(4) unsigned DEFAULT '0',
  `StockMinimo` int(4) unsigned DEFAULT '0',
  `StockMaximo` int(4) unsigned DEFAULT '0',
  FechaEdicion date,
  FOREIGN KEY (`IdEditorial`) REFERENCES `Editoriales` (`IdEditorial`)
  ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`IdTema`) REFERENCES `Temas` (`IdTema`)
  ON DELETE CASCADE ON UPDATE CASCADE
  -- CONSTRAINT Id_Libros_PK PRIMARY KEY (Id),
  -- CONSTRAINT Libros_Editoriales_FK FOREIGN KEY (Editoriales) REFERENCES Editoriales (IdEditorial),
  -- CONSTRAINT Libros_Temas_FK FOREIGN KEY (Temas) REFERENCES Temas (IdTema)
);

