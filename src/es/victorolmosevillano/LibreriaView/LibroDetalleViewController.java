/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.LibreriaView;

import es.victorolmosevillano.libreria.Editoriales;
import es.victorolmosevillano.libreria.Libros;
import es.victorolmosevillano.libreria.Temas;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * FXML Controller class
 *
 * @author Victor
 */
public class LibroDetalleViewController implements Initializable {

    private Pane rootLibreriaView;
    private TableView listViewPrevio;
    private Libros libro;
    private Editoriales Editorial;
    private Temas tema;
    private EntityManager entityManager;
    private boolean nuevoLibro;
    @FXML
    private Button buttonGuardar;
    @FXML
    private Button buttonCancelar;
    @FXML
    private AnchorPane rootLibroDetalleView;
    @FXML
    private ListView<Editoriales> listViewEditoriales;
    @FXML
    private ListView<Temas> listViewTemas;
    private ComboBox<Editoriales> comboBoxEditorial;
    private ComboBox<Temas> comboBoxTemas;
    private Libros libroSeleccionado;
    LibreriaViewController libreriaViewController = new LibreriaViewController();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }



    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;

    }

    /* public void setEditoriales(EntityManager entityManager) {
                this.entityManager = entityManager;
    }*/
    public void cargarDBeditorialesTemas() {
        Query queryEditorialesFindAll = entityManager.createNamedQuery("Editoriales.findAll");
        List<Editoriales> listEditoriales = queryEditorialesFindAll.getResultList();
        listViewEditoriales.setItems(FXCollections.observableArrayList(listEditoriales));
        listViewEditoriales.setCellFactory(new StringListCellFactory());

        Query queryTemasFindAll = entityManager.createNamedQuery("Temas.findAll");
        List<Temas> listTemas = queryTemasFindAll.getResultList();
        listViewTemas.setItems(FXCollections.observableArrayList(listTemas));
        listViewTemas.setCellFactory(new StringListCellFactory2());
    }

    class StringListCellFactory implements
            Callback<ListView<Editoriales>, ListCell<Editoriales>> {

        @Override
        public ListCell<Editoriales> call(ListView<Editoriales> playerListView) {
            return new StringListCell();
        }

        class StringListCell extends ListCell<Editoriales> {

            @Override
            protected void updateItem(Editoriales editorial, boolean b) {
                super.updateItem(editorial, b);

                if (editorial != null) {
                    setText(editorial.getNombre());
                }

            }
        }
    }

    class StringListCellFactory2 implements
            Callback<ListView<Temas>, ListCell<Temas>> {

        @Override
        public ListCell<Temas> call(ListView<Temas> playerListView) {
            return new StringListCell();
        }

        class StringListCell extends ListCell<Temas> {

            @Override
            protected void updateItem(Temas tema, boolean b) {
                super.updateItem(tema, b);

                if (tema != null) {
                    setText(tema.getTema());
                }

            }
        }
    }

    public void setRootLibreriaView(Pane rootContactosView) {
        this.rootLibreriaView = rootContactosView;
    }

    public void setLibroSeleccionado(Libros libroSeleccionado) {
        this.libroSeleccionado = libroSeleccionado;

    }
        public void setComboBoxEditorialTema(ComboBox<Editoriales> comboBoxEditorial, ComboBox<Temas> comboBoxTemas) {
        this.comboBoxEditorial = comboBoxEditorial;
        this.comboBoxTemas = comboBoxTemas;
    }
   // int numListSeleccionado;
    @FXML
    private void onActionButtonGuardar(ActionEvent event) {

        StackPane rootMain = (StackPane) rootLibroDetalleView.getScene().getRoot();
        rootMain.getChildren().remove(rootLibroDetalleView);
        rootLibreriaView.setVisible(true);

        entityManager.getTransaction().begin();
        // System.out.println(libroSeleccionado);

        // Esto RETORNA la Editorial que se encuentre selecciona
        // Hay que asignarle al libro la editorial
        // listViewEditoriales.setSelectionModel();
        libroSeleccionado.setIdEditorial(listViewEditoriales.getSelectionModel().getSelectedItem());

        libroSeleccionado.setIdTema(listViewTemas.getSelectionModel().getSelectedItem());

        entityManager.merge(libroSeleccionado);
        entityManager.getTransaction().commit();
       

        //  listViewEditoriales.getItems().set(numListSeleccionado, Editorial);
        // listViewTemas.getItems().set(numListSeleccionado, tema);
        //    libreriaViewController.setComboBoxEditorialTema(comboBoxEditorial, comboBoxTemas);
       // System.out.println(comboBoxEditorial);
        comboBoxEditorial.setValue(listViewEditoriales.getSelectionModel().getSelectedItem());
        comboBoxTemas.setValue(listViewTemas.getSelectionModel().getSelectedItem());
             //     int numFilaSeleccionada = comboBoxEditorial.getSelectionModel().getSelectedIndex();
             //   comboBoxEditorial.getItems().set(Editorial, libroSeleccionado);
            
    }

    @FXML
    private void onActionButtonCancelar(ActionEvent event) {

        StackPane rootMain = (StackPane) rootLibroDetalleView.getScene().getRoot();
        rootMain.getChildren().remove(rootLibroDetalleView);
        rootLibreriaView.setVisible(true);
        //   entityManager.getTransaction().rollback();
    }

}
