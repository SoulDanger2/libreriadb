/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.LibreriaView;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Victor
 */
public class Ventana extends Application {

    private EntityManagerFactory emf;
    private EntityManager em;
    public static Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws IOException {
        StackPane rootMain = new StackPane();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LibreriaView.fxml"));
        Pane rootLibreriaView = fxmlLoader.load();
        rootMain.getChildren().add(rootLibreriaView);

        emf = Persistence.createEntityManagerFactory("LibreriaPU");
        em = emf.createEntityManager();
        LibreriaViewController libreriaViewController = (LibreriaViewController) fxmlLoader.getController();
//       LibroDetalleViewController libroDetalleViewController = (LibroDetalleViewController) fxmlLoader.getController();
        // Después de obtener el objeto del controlador y del EntityManager:
        libreriaViewController.setEntityManager(em);
       // libroDetalleViewController.setEditoriales(em);
        libreriaViewController.cargarDBLibros();
       
        Scene scene = new Scene(rootMain, 650, 580);
        
        primaryStage.setTitle("DB Libreria (Todos los derechos reservados a Victor M. Olmo)");
        primaryStage.setScene(scene);
        primaryStage.show();
        this.primaryStage = primaryStage;
    }

    @Override
    public void stop() throws Exception {
        em.close();
        emf.close();
        try {
            DriverManager.getConnection("jdbc:mysql://localhost:3306/mysql?zeroDateTimeBehavior=convertToNull");
        } catch (SQLException ex) {
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
