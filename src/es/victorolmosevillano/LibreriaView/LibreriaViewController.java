/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.LibreriaView;

import es.victorolmosevillano.libreria.Editoriales;
import es.victorolmosevillano.libreria.Libros;
import es.victorolmosevillano.libreria.Temas;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * FXML Controller class
 *
 * @author Victor
 */
public class LibreriaViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private EntityManager entityManager;
    private Libros libroSeleccionado;
    private Libros libro;
    private boolean nuevoLibro;
    // private ListView listViewPrevio;
    // private Pane rootContactosView;

    @FXML
    private ListView<Libros> listViewLibros;
    @FXML
    private Button buttonNuevo;
    @FXML
    private Button buttonBorrar;
    @FXML
    private TextField textFieldTitulo;
    @FXML
    private TextField textFieldAutor;

    @FXML
    private TextField textFieldPrecioCoste;
    @FXML
    private TextField textFieldPvPsinIvA;
    @FXML
    private TextField textFieldStockActual;
    @FXML
    private TextField textFieldStockMinimo;
    @FXML
    private TextField textFieldStockMaximo;

    @FXML
    private ComboBox<Editoriales> comboBoxEditorial;
    @FXML
    private ComboBox<Temas> comboBoxTemas;
    @FXML
    private AnchorPane rootLibreriaView;
    @FXML
    private Button buttonSeleccionar;
    @FXML
    private Button buttonGuardar;
    @FXML
    private DatePicker datePickerFechaEdicion;
    @FXML
    private Label labelCuentaLibros;
    @FXML
    private Label labelCuentaRegistros;
    @FXML
    private Button bottomSalir;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    EntityManagerFactory emf = Persistence
            .createEntityManagerFactory("LibreriaPU");
    EntityManager em = emf.createEntityManager();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        limitTextField(textFieldAutor, 80);
        limitTextField(textFieldTitulo, 80);
        limitTextField(textFieldPrecioCoste, 5);
        limitTextField(textFieldPvPsinIvA, 5);
        limitTextField(textFieldStockActual, 4);
        limitTextField(textFieldStockMaximo, 4);
        limitTextField(textFieldStockMinimo, 4);

        editableFalseTextField();

        labelCuentaLibros();
        labelCuentaRegistros();

        // TODO      
        listViewLibros.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    libroSeleccionado = newValue;
                    if (libroSeleccionado != null) {
                        //   int fontSize = 20;
                        //   Background background = (Background)20;
                        //  textFieldAutor.setBackground(Background.EMPTY);
                        textFieldTitulo.setText(libroSeleccionado.getTitulo());
                        textFieldAutor.setText(libroSeleccionado.getAutores());
                        textFieldPrecioCoste.setText(libroSeleccionado.getPrecioCoste().toString());
                        textFieldPvPsinIvA.setText(libroSeleccionado.getPvpSinIva().toString());
                        textFieldStockActual.setText(libroSeleccionado.getStockActual().toString());
                        textFieldStockMinimo.setText(libroSeleccionado.getStockMinimo().toString());
                        textFieldStockMaximo.setText(libroSeleccionado.getStockMaximo().toString());

                        mostrarDatos();

                    } else {
                        textFieldTitulo.setText("");
                        textFieldAutor.setText("");
                        textFieldPrecioCoste.setText("");
                        textFieldPvPsinIvA.setText("");
                        textFieldStockActual.setText("");
                        textFieldStockMinimo.setText("");
                        textFieldStockMaximo.setText("");
                        datePickerFechaEdicion.setValue(null);
                    }
                });

        listViewLibros.setCellFactory(new Callback<ListView<Libros>, ListCell<Libros>>() {

            @Override
            public ListCell<Libros> call(ListView<Libros> param) {
                ListCell<Libros> cell = new ListCell<Libros>() {

                    @Override
                    protected void updateItem(Libros libro, boolean empty) {
                        super.updateItem(libro, empty);
                        if (libro != null) {
                            setText(libro.getTitulo() + "\n" + libro.getAutores());
                        } else {
                            setText("");   // <== clear the now empty cell.
                        }
                    }
                };
                return cell;
            }
        });

        comboBoxEditorial.setConverter(new StringConverter<Editoriales>() {
            @Override
            public String toString(Editoriales editoriales) {
                if (editoriales == null) {
                    return null;
                } else {
                    return editoriales.getNombre();
                }
            }

            @Override
            public Editoriales fromString(String userId) {
                return null;
            }
        });

        //como muestra la lista de editoriales.
        comboBoxEditorial.setCellFactory((ListView<Editoriales> l) -> new ListCell<Editoriales>() {
            @Override
            protected void updateItem(Editoriales editoriales, boolean empty) {
                super.updateItem(editoriales, empty);
                if (editoriales == null || empty) {
                    setText("");
                } else {
                    setText(editoriales.getNombre());
                }
            }
        });

        comboBoxTemas.setConverter(new StringConverter<Temas>() {
            @Override
            public String toString(Temas temas) {
                if (temas == null) {
                    return null;
                } else {
                    return temas.getTema();
                }
            }

            @Override
            public Temas fromString(String userId) {
                return null;
            }
        });

        comboBoxTemas.setCellFactory((ListView<Temas> l) -> new ListCell<Temas>() {
            @Override
            protected void updateItem(Temas temas, boolean empty) {
                super.updateItem(temas, empty);
                if (temas == null || empty) {
                    setText("");
                } else {
                    setText(temas.getTema());
                }
            }
        });

    }

    public static void limitTextField(TextField textField, int limit) {
        UnaryOperator<Change> textLimitFilter = change -> {
            if (change.isContentChange()) {
                int newLength = change.getControlNewText().length();
                if (newLength > limit) {
                    String trimmedText = change.getControlNewText().substring(0, limit);
                    change.setText(trimmedText);
                    int oldLength = change.getControlText().length();
                    change.setRange(0, oldLength);
                }
            }
            return change;
        };
        textField.setTextFormatter(new TextFormatter(textLimitFilter));
    }

    public static void integerTextField(TextField textField) {
        UnaryOperator<Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            System.out.println("newText " + newText);
            if (newText.matches("-?([0-9]*)?")) {
                System.out.println("cumple regla");
                return change;
            }
            System.out.println("No cumple regla");
            return null;
        };
        textField.setTextFormatter(
                new TextFormatter<Integer>(
                        new IntegerStringConverter(), 0, integerFilter));
    }

    public void labelCuentaLibros() {
        Query queryStock = em.createNativeQuery("SELECT SUM(StockActual) FROM Libros");
        BigDecimal stockActual = (BigDecimal) queryStock.getSingleResult();
        labelCuentaLibros.setText("El Stock Actual es tu libreria es de " + stockActual + " libros en total.");
    }

    public void labelCuentaRegistros() {
        Query queryCuentaRegistros = em.createNativeQuery("SELECT COUNT(IdLibro) FROM Libros");
        Long cuentaRegistros = (Long) queryCuentaRegistros.getSingleResult();
        labelCuentaRegistros.setText("Actualmente tienes un total de " + cuentaRegistros + " registros en la DB.");
    }

    class StringListCellFactory implements
            Callback<ListView<Libros>, ListCell<Libros>> {

        @Override
        public ListCell<Libros> call(ListView<Libros> playerListView) {
            return new StringListCell();
        }

        class StringListCell extends ListCell<Libros> {

            @Override
            protected void updateItem(Libros libros, boolean b) {
                super.updateItem(libros, b);

                if (libros != null) {
                    setText(libros.getTitulo() + "\n" + libros.getAutores() + "\n");
                }

            }
        }
    }

    public void cargarDBLibros() {
        Query queryLibrosFindAll = entityManager.createNamedQuery("Libros.findAll");
        List<Libros> listLibros = queryLibrosFindAll.getResultList();
        listViewLibros.setItems(FXCollections.observableArrayList(listLibros));

    }

    public void setLibro(Libros libro) {

        this.libro = libro;

    }

    public void editableTrueTextField() {
        textFieldTitulo.setEditable(true);
        textFieldAutor.setEditable(true);
        textFieldPrecioCoste.setEditable(true);
        textFieldPvPsinIvA.setEditable(true);
        textFieldStockActual.setEditable(true);
        textFieldStockMaximo.setEditable(true);
        textFieldStockMinimo.setEditable(true);
        // comboBoxEditorial.setEditable(true);
        //  comboBoxTemas.setEditable(true);
        datePickerFechaEdicion.setEditable(true);
    }

    public void editableFalseTextField() {
        textFieldTitulo.setEditable(false);
        textFieldAutor.setEditable(false);
        textFieldPrecioCoste.setEditable(false);
        textFieldPvPsinIvA.setEditable(false);
        textFieldStockActual.setEditable(false);
        textFieldStockMaximo.setEditable(false);
        textFieldStockMinimo.setEditable(false);
        comboBoxEditorial.setEditable(false);
        comboBoxTemas.setEditable(false);
        datePickerFechaEdicion.setEditable(false);
    }

    public void mostrarDatos() {

        editableTrueTextField();

        Query queryEditorialesFindAll = entityManager.createNamedQuery("Editoriales.findAll");
        List listEditoriales = queryEditorialesFindAll.getResultList();
        comboBoxEditorial.setItems(FXCollections.observableList(listEditoriales));

        Query queryTemasFindAll = entityManager.createNamedQuery("Temas.findAll");
        List listTemas = queryTemasFindAll.getResultList();
        comboBoxTemas.setItems(FXCollections.observableList(listTemas));
        textFieldTitulo.setText(libroSeleccionado.getTitulo());
        textFieldAutor.setText(libroSeleccionado.getAutores());

        //como muestra como aparece la editorial.
        if (libroSeleccionado.getIdEditorial() != null) {
            comboBoxEditorial.setValue(libroSeleccionado.getIdEditorial());
        }

        if (libroSeleccionado.getIdTema() != null) {
            comboBoxTemas.setValue(libroSeleccionado.getIdTema());
        }

        if (libroSeleccionado.getPrecioCoste() != null) {
            textFieldPrecioCoste.setText(libroSeleccionado.getPrecioCoste().toString());
        }

        if (libroSeleccionado.getPvpSinIva() != null) {
            textFieldPvPsinIvA.setText(libroSeleccionado.getPvpSinIva().toString());
        }

        if (libroSeleccionado.getStockActual() != null) {
            textFieldStockActual.setText(libroSeleccionado.getStockActual().toString());
        }

        if (libroSeleccionado.getStockMinimo() != null) {
            textFieldStockMinimo.setText(libroSeleccionado.getStockMinimo().toString());
        }

        if (libroSeleccionado.getStockMaximo() != null) {
            textFieldStockMaximo.setText(libroSeleccionado.getStockMaximo().toString());
        }
        if (libroSeleccionado.getFechaEdicion() != null) {
            Date date = libroSeleccionado.getFechaEdicion();
            Instant instant = date.toInstant();
            ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
            LocalDate localDate = zdt.toLocalDate();
            datePickerFechaEdicion.setValue(localDate);
        }

    }

    @FXML
    private void onActionButtonNuevo(ActionEvent event) {

        editableTrueTextField();
        // try {
        // Cargar la vista de detalle
        //  FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LibroDetalleView.fxml"));
        //  Parent rootLibreriaDetalleView = fxmlLoader.load();
        //  LibroDetalleViewController libroDetalleViewController = (LibroDetalleViewController) fxmlLoader.getController();
        //   libroDetalleViewController.setRootLibreriaView((Pane)rootLibreriaView);
        // libroDetalleViewController.setRootLibreriaView(listViewLibros);
        //   libroDetalleViewController.setRootLibreriaView(rootLibreriaView);

        // Código para el botón Nuevo
        //libroSeleccionado = new Libros();
        //   libroDetalleViewController.setRootLibreriaView(entityManager, libroSeleccionado, true);
        // Ocultar la vista de la lista
        //     rootLibreriaView.setVisible(false);
        // Añadir la vista de detalle al StackPane principal para que se muestre
        //  StackPane rootMain = (StackPane)rootLibreriaView.getScene().getRoot();
        //  rootMain.getChildren().add(rootLibreriaDetalleView);
        setLibro(libro);

        Query queryEditorialesFindAll = entityManager.createNamedQuery("Editoriales.findAll");
        List listEditoriales = queryEditorialesFindAll.getResultList();
        comboBoxEditorial.setItems(FXCollections.observableList(listEditoriales));

        Query queryTemasFindAll = entityManager.createNamedQuery("Temas.findAll");
        List listTemas = queryTemasFindAll.getResultList();
        comboBoxTemas.setItems(FXCollections.observableList(listTemas));

        //           mostrarDatos();
        nuevoLibro = true;

        textFieldTitulo.setText("");
        textFieldAutor.setText("");
        comboBoxEditorial.setValue(null);
        comboBoxTemas.setValue(null);
        textFieldPrecioCoste.setText("");
        textFieldPvPsinIvA.setText("");
        textFieldStockActual.setText("");
        textFieldStockMinimo.setText("");
        textFieldStockMaximo.setText("");
        //   textFieldFechaEdicion.setText("");

        /*   } catch (IOException ex) {
            Logger.getLogger(LibreriaViewController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        listViewLibros.getSelectionModel().select(null);
    }

    @FXML
    private void onActionButtonBorrar(ActionEvent event) {
        if (libroSeleccionado == null) {
            Alert alertFallo = new Alert(AlertType.WARNING);
            alertFallo.setContentText("No has Seleccionado Registro!!!!");
            Optional<ButtonType> result = alertFallo.showAndWait();

        } else {
            Alert alertBorrar = new Alert(AlertType.CONFIRMATION);
            alertBorrar.setTitle("Confirmar");
            alertBorrar.setHeaderText("¿Deseas Borrar el siguiente libro?");
            alertBorrar.setContentText(libroSeleccionado.getTitulo());
            Optional<ButtonType> result = alertBorrar.showAndWait();
            if (result.get() == ButtonType.OK) {
                // Acciones a realizar si el usuario acepta
                entityManager.getTransaction().begin();
                entityManager.remove(libroSeleccionado);
                entityManager.getTransaction().commit();

                listViewLibros.getItems().remove(libroSeleccionado);
                listViewLibros.refresh();
                //listViewLibros.getFocusModel().focus(null);
                //listViewLibros.requestFocus();
                textFieldTitulo.setText("");
                textFieldAutor.setText("");
                comboBoxEditorial.setValue(null);
                comboBoxTemas.setValue(null);
                textFieldPrecioCoste.setText("");
                textFieldPvPsinIvA.setText("");
                textFieldStockActual.setText("");
                textFieldStockMinimo.setText("");
                textFieldStockMaximo.setText("");
                datePickerFechaEdicion.setValue(null);

                editableFalseTextField();

            } else {
                // Acciones a realizar si el usuario cancela
                int numListSeleccionado = listViewLibros.getSelectionModel().getSelectedIndex();
                listViewLibros.getItems().set(numListSeleccionado, libroSeleccionado);
                listViewLibros.getFocusModel().focus(numListSeleccionado);
                listViewLibros.requestFocus();
            }

            labelCuentaLibros();
            labelCuentaRegistros();
        }
    }

    @FXML
    private void onActionButtonGuardar(ActionEvent event) {
        boolean errorFormato = false;
        entityManager.getTransaction().begin();
        if (nuevoLibro != true) {

            if (libroSeleccionado != null) {

                System.out.println("modificar");
                libroSeleccionado.setTitulo(textFieldTitulo.getText());

                libroSeleccionado.setAutores(textFieldAutor.getText());

                if (!textFieldPrecioCoste.getText().isEmpty()) {
                    try {
                        libroSeleccionado.setPrecioCoste(BigDecimal.valueOf(Double.valueOf(textFieldPrecioCoste.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "PrecioCoste no válido");
                        alert.showAndWait();
                        textFieldPrecioCoste.requestFocus();
                    }
                }

                if (!textFieldPvPsinIvA.getText().isEmpty()) {
                    try {
                        libroSeleccionado.setPvpSinIva(BigDecimal.valueOf(Double.valueOf(textFieldPvPsinIvA.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "PvPsinIvA no válido");
                        alert.showAndWait();
                        textFieldPvPsinIvA.requestFocus();
                    }
                }

                if (!textFieldStockActual.getText().isEmpty()) {
                    try {
                        libroSeleccionado.setStockActual(Integer.valueOf(textFieldStockActual.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockActual.requestFocus();
                    }
                }

                if (!textFieldStockMinimo.getText().isEmpty()) {
                    try {
                        libroSeleccionado.setStockMinimo(Integer.valueOf(textFieldStockMinimo.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockMinimo.requestFocus();
                    }

                }

                if (!textFieldStockMaximo.getText().isEmpty()) {
                    try {
                        libroSeleccionado.setStockMaximo(Integer.valueOf(textFieldStockMaximo.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockMaximo.requestFocus();
                    }

                }

                if (datePickerFechaEdicion.getValue() != null) {
                    LocalDate localDate = datePickerFechaEdicion.getValue();
                    ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
                    Instant instant = zonedDateTime.toInstant();
                    Date date = Date.from(instant);
                    libroSeleccionado.setFechaEdicion(date);
                } else {
                    libroSeleccionado.setFechaEdicion(null);
                }

                entityManager.merge(libroSeleccionado);
                //  entityManager.getTransaction().commit();
                int numFilaSeleccionada = listViewLibros.getSelectionModel().getSelectedIndex();
                listViewLibros.getItems().set(numFilaSeleccionada, libroSeleccionado);

                listViewLibros.requestFocus();
                entityManager.merge(libroSeleccionado);

            }
            entityManager.getTransaction().commit();

            editableTrueTextField();

        } else {
            System.out.println("Añadir");
            int libroSeleccionado;

            //   entityManager.getTransaction().begin();
            if (nuevoLibro) {
                libro = new Libros();
                libro.setTitulo(textFieldTitulo.getText());
                libro.setAutores(textFieldAutor.getText());
                if (!textFieldPrecioCoste.getText().isEmpty()) {
                    try {
                        libro.setPrecioCoste(BigDecimal.valueOf(Double.valueOf(textFieldPrecioCoste.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "PrecioCoste no válido");
                        alert.showAndWait();
                        textFieldPrecioCoste.requestFocus();
                    }
                }

                if (!textFieldPvPsinIvA.getText().isEmpty()) {
                    try {
                        libro.setPvpSinIva(BigDecimal.valueOf(Double.valueOf(textFieldPvPsinIvA.getText()).doubleValue()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "PvPsinIvA no válido");
                        alert.showAndWait();
                        textFieldPvPsinIvA.requestFocus();
                    }
                }

                if (!textFieldStockActual.getText().isEmpty()) {
                    try {
                        libro.setStockActual(Integer.valueOf(textFieldStockActual.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockActual.requestFocus();
                    }
                }

                if (!textFieldStockMinimo.getText().isEmpty()) {
                    try {
                        libro.setStockMinimo(Integer.valueOf(textFieldStockMinimo.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockMinimo.requestFocus();
                    }

                }

                if (!textFieldStockMaximo.getText().isEmpty()) {
                    try {
                        libro.setStockMaximo(Integer.valueOf(textFieldStockMaximo.getText()));
                    } catch (NumberFormatException ex) {
                        errorFormato = true;
                        Alert alert = new Alert(AlertType.INFORMATION, "StockActual no válido");
                        alert.showAndWait();
                        textFieldStockMaximo.requestFocus();
                    }

                }

                if (datePickerFechaEdicion.getValue() != null) {
                    LocalDate localDate = datePickerFechaEdicion.getValue();
                    ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
                    Instant instant = zonedDateTime.toInstant();
                    Date date = Date.from(instant);
                    libro.setFechaEdicion(date);
                } else {
                    libro.setFechaEdicion(null);
                }

                if (comboBoxEditorial.getValue() != null) {
                    libro.setIdEditorial(comboBoxEditorial.getValue());
                } else {
                    Alert alert = new Alert(AlertType.INFORMATION, "Debe indicar una Editorial");
                    alert.showAndWait();
                    errorFormato = true;
                }

                if (comboBoxTemas.getValue() != null) {
                    libro.setIdTema(comboBoxTemas.getValue());
                } else {
                    Alert alert = new Alert(AlertType.INFORMATION, "Debe indicar un Tema");
                    alert.showAndWait();
                    errorFormato = true;
                }

                listViewLibros.getItems().add(libro);
            }

            entityManager.persist(libro);
            entityManager.getTransaction().commit();

            editableFalseTextField();

        }
        //   entityManager.getTransaction().commit();
        nuevoLibro = false;

        labelCuentaLibros();
        labelCuentaRegistros();

    }

    public void setComboBoxEditorialTema(ComboBox<Editoriales> comboBoxEditorial, ComboBox<Temas> Temas) {
        this.comboBoxEditorial = comboBoxEditorial;
        this.comboBoxTemas = comboBoxTemas;
    }

    @FXML
    private void onActionButtonSeleccionar(ActionEvent event) {

        try {
            // Cargar la vista de detalle
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LibroDetalleView.fxml"));
            Parent rootDetalleView = fxmlLoader.load();
            LibroDetalleViewController libroDetalleViewController = (LibroDetalleViewController) fxmlLoader.getController();
            libroDetalleViewController.setRootLibreriaView((Pane) rootLibreriaView);

            libroDetalleViewController.setEntityManager(entityManager);
            libroDetalleViewController.cargarDBeditorialesTemas();
            System.out.println("Pasar el libro a editar: " + libroSeleccionado);
            libroDetalleViewController.setLibroSeleccionado(libroSeleccionado);

            libroDetalleViewController.setComboBoxEditorialTema(comboBoxEditorial, comboBoxTemas);

            // Ocultar la vista de la lista
            rootLibreriaView.setVisible(false);

            // Añadir la vista de detalle al StackPane principal para que se muestre
            StackPane rootMain = (StackPane) rootLibreriaView.getScene().getRoot();
            rootMain.getChildren().add(rootDetalleView);
        } catch (IOException ex) {
            Logger.getLogger(LibreriaViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onActionButtonSalir(ActionEvent event) {
        Ventana.primaryStage.close();
    }

}
