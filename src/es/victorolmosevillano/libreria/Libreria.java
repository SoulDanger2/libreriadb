/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.libreria;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Victor
 */
public class Libreria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Map<String, String> emfProperties = new HashMap<String, String>();
        // emfProperties.put("javax.persistence.jdbc.user", "root");
        // emfProperties.put("javax.persistence.jdbc.password", "root");
        emfProperties.put("javax.persistence.schema-generation.database.action", "create");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("LibreriaPU");
        EntityManager em = emf.createEntityManager();
        emfProperties.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/mysql?zeroDateTimeBehavior=convertToNull");
        
        // REALIZAR AQUÍ LAS OPERACIONES SOBRE LA BASE DE DATOS
                //transacciones.
        /*Se puede obtener iniciar una transacción sobre dicho EntityManager 
         con una sentencia como esta:*/
        //para iniciar las transacciones.
        Temas temaInformatica = new Temas(1, "Informatica");
        Temas temaAstronomia = new Temas(2, "Astronomia");
        Temas temaMatematicas = new Temas();
        temaMatematicas.setTema("Matematicas");
        //transacciones.
        /*Se puede obtener iniciar una transacción sobre dicho EntityManager 
         con una sentencia como esta:*/
        //para iniciar las transacciones.
        em.getTransaction().begin();
        // Añadir aquí las operaciones de modificación de la base de datos
        em.persist(temaInformatica);
        em.persist(temaAstronomia);

        em.persist(temaMatematicas);

        //Realiza volcado definitivo a la base de datos (guarda las transacciones realizadas).
        em.getTransaction().commit();
        
        // Cerrar la conexión con la base de datos
        em.close(); 
        emf.close(); 
        try { 
            DriverManager.getConnection("jdbc:mysql://localhost:3306/mysql?zeroDateTimeBehavior=convertToNull"); 
        } catch (SQLException ex) { 
        }
    }

}
