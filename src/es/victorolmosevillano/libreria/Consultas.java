/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.libreria;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Victor
 */
public class Consultas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("LibreriaPU");
        EntityManager em = emf.createEntityManager();
        // TODO code application logic here
        System.out.println("Consulta 1");
        System.out.println("----------");
        Query queryTemas = em.createNamedQuery("Temas.findAll");
        List<Temas> listTemas = queryTemas.getResultList();
        System.out.println("Los Temas que existen en la base de datos son:");
        for (Temas temas : listTemas) {
            System.out.println(temas.getTema());
        }

        System.out.println(" ");
        System.out.println("Consulta 2");
        System.out.println("----------");
        System.out.println("Otra Forma de mostrar una lista:");
        for (int i = 0; i < listTemas.size(); i++) {
            Temas provincia = listTemas.get(i);
            System.out.println(provincia.getTema());
        }

        System.out.println(" ");
        System.out.println("Consulta 3");
        System.out.println("----------");
        Query queryTemaInformatica = em.createNamedQuery("Temas.findByTema");
        queryTemaInformatica.setParameter("tema", "Informatica");
        List<Temas> listTemaInformatica = queryTemaInformatica.getResultList();
        for (Temas temaInformatica : listTemaInformatica) {
            System.out.print("El tema con id ");
            System.out.print(temaInformatica.getIdTema() + " es: ");
            System.out.println(temaInformatica.getTema());
        }

        System.out.println(" ");
        System.out.println("Consulta 4");
        System.out.println("----------");
        Query queryStock = em.createNativeQuery("SELECT SUM(StockActual) FROM Libros");
        BigDecimal stockActual = (BigDecimal) queryStock.getSingleResult();
        System.out.println("El Stock Actual de todos los libros de tu tienda es " + stockActual);

        System.out.println(" ");
        System.out.println("Consulta 5");
        System.out.println("----------");
        Query queryCuentaRegistros = em.createNativeQuery("SELECT COUNT(IdLibro) FROM Libros");
        Long cuentaRegistros = (Long) queryCuentaRegistros.getSingleResult();
        System.out.println("Tienes un total de " + cuentaRegistros + " registros en la base de datos.");
    }

}
