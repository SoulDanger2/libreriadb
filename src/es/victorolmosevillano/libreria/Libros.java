/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.libreria;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Victor
 */
@Entity
@Table(name = "libros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Libros.findAll", query = "SELECT l FROM Libros l")
    , @NamedQuery(name = "Libros.findByIdLibro", query = "SELECT l FROM Libros l WHERE l.idLibro = :idLibro")
    , @NamedQuery(name = "Libros.findByTitulo", query = "SELECT l FROM Libros l WHERE l.titulo = :titulo")
    , @NamedQuery(name = "Libros.findByAutores", query = "SELECT l FROM Libros l WHERE l.autores = :autores")
    , @NamedQuery(name = "Libros.findByPrecioCoste", query = "SELECT l FROM Libros l WHERE l.precioCoste = :precioCoste")
    , @NamedQuery(name = "Libros.findByPvpSinIva", query = "SELECT l FROM Libros l WHERE l.pvpSinIva = :pvpSinIva")
    , @NamedQuery(name = "Libros.findByStockActual", query = "SELECT l FROM Libros l WHERE l.stockActual = :stockActual")
    , @NamedQuery(name = "Libros.findByStockMinimo", query = "SELECT l FROM Libros l WHERE l.stockMinimo = :stockMinimo")
    , @NamedQuery(name = "Libros.findByStockMaximo", query = "SELECT l FROM Libros l WHERE l.stockMaximo = :stockMaximo")
    , @NamedQuery(name = "Libros.findByFechaEdicion", query = "SELECT l FROM Libros l WHERE l.fechaEdicion = :fechaEdicion")})
public class Libros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdLibro")
    private Integer idLibro;
    @Basic(optional = false)
    @Column(name = "Titulo")
    private String titulo;
    @Basic(optional = false)
    @Column(name = "Autores")
    private String autores;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PrecioCoste")
    private BigDecimal precioCoste;
    @Column(name = "PvpSinIva")
    private BigDecimal pvpSinIva;
    @Column(name = "StockActual")
    private Integer stockActual;
    @Column(name = "StockMinimo")
    private Integer stockMinimo;
    @Column(name = "StockMaximo")
    private Integer stockMaximo;
    @Column(name = "FechaEdicion")
    @Temporal(TemporalType.DATE)
    private Date fechaEdicion;
    @JoinColumn(name = "IdEditorial", referencedColumnName = "IdEditorial")
    @ManyToOne
    private Editoriales idEditorial;
    @JoinColumn(name = "IdTema", referencedColumnName = "IdTema")
    @ManyToOne
    private Temas idTema;

    public Libros() {
    }

    public Libros(Integer idLibro) {
        this.idLibro = idLibro;
    }

    public Libros(Integer idLibro, String titulo, String autores) {
        this.idLibro = idLibro;
        this.titulo = titulo;
        this.autores = autores;
    }

    public Integer getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(Integer idLibro) {
        this.idLibro = idLibro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutores() {
        return autores;
    }

    public void setAutores(String autores) {
        this.autores = autores;
    }

    public BigDecimal getPrecioCoste() {
        return precioCoste;
    }

    public void setPrecioCoste(BigDecimal precioCoste) {
        this.precioCoste = precioCoste;
    }

    public BigDecimal getPvpSinIva() {
        return pvpSinIva;
    }

    public void setPvpSinIva(BigDecimal pvpSinIva) {
        this.pvpSinIva = pvpSinIva;
    }

    public Integer getStockActual() {
        return stockActual;
    }

    public void setStockActual(Integer stockActual) {
        this.stockActual = stockActual;
    }

    public Integer getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(Integer stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public Integer getStockMaximo() {
        return stockMaximo;
    }

    public void setStockMaximo(Integer stockMaximo) {
        this.stockMaximo = stockMaximo;
    }

    public Date getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(Date fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public Editoriales getIdEditorial() {
        return idEditorial;
    }

    public void setIdEditorial(Editoriales idEditorial) {
        this.idEditorial = idEditorial;
    }

    public Temas getIdTema() {
        return idTema;
    }

    public void setIdTema(Temas idTema) {
        this.idTema = idTema;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLibro != null ? idLibro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Libros)) {
            return false;
        }
        Libros other = (Libros) object;
        if ((this.idLibro == null && other.idLibro != null) || (this.idLibro != null && !this.idLibro.equals(other.idLibro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "es.victorolmosevillano.libreria.Libros[ idLibro=" + idLibro + " ]";
    }
    
}
